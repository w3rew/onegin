/*!
 \file str_utils.h This file contains definitions of functions, used to read and sort files
 */
#ifndef _STR_UTILS_H
#define _STR_UTILS_H
/*!
 A struct which contains string and its length
 \warning String might not have the terminating 0!
 */
typedef struct line {
    const char* str; ///< A string
    size_t len; ///< Length of the string
} Line;
char* file_to_str(size_t *n, FILE *stream);
ssize_t str_to_lines(const char* str, size_t len, Line** lines, size_t* n);
int line_comp(const void* a, const void* b);
int rev_line_comp(const void* a, const void* b);
void my_qsort (void* base, size_t num, size_t size, int (*compar)(const void*, const void*));
#endif
