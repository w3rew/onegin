/*!
 \file onegin.c Here we sort a file string by string and print it to stdout
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "str_utils.h"
#define CALLOC_LINES(num) ((Line*) calloc(((num)), sizeof(Line))) //no point in using calloc
#define MY_QSORT my_qsort
/*!
 This function prints Line
 \param[in] line a Line to print
 */
void print_line(Line line) {
    for (size_t i = 0; i < line.len; ++i) printf("%c", line.str[i]);
}
/*!
 This functions prints array of n Lines
 \param[in] lines an array of Lines
 \param[in] n size of the array
 */
void print_lines(Line* lines, size_t n) {
    for (size_t i = 0; i < n; ++i) {
        print_line(lines[i]);
        printf("\n");
    }
}
/*!
 Here we at first read file, then sort it and prints to stdout. After all, we clean memory.
 */
int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Provide file name as argument!\n");
        return 1;
    }
    FILE* file = fopen(argv[1], "r");

    size_t read_chars = 0;
    char* text = file_to_str(&read_chars, file);
    fclose(file);
    if (text == NULL) return -1;

    size_t allocated_lines = 0;
    Line* lines = NULL;
    ssize_t converted_lines = str_to_lines(text, read_chars, &lines, &allocated_lines);
    if (converted_lines < 0){
        free(text);
        free(lines);
        return -1;
    }

    Line* sorted_lines = CALLOC_LINES((size_t) converted_lines);
    if (sorted_lines == NULL) return -1;
    Line* rev_sorted_lines = CALLOC_LINES((size_t) converted_lines);
    if (rev_sorted_lines == NULL) return -1;
    memcpy(sorted_lines, lines, sizeof(Line) * ((size_t) converted_lines));
    memcpy(rev_sorted_lines, lines, sizeof(Line) * ((size_t) converted_lines));

    MY_QSORT(sorted_lines, (size_t) converted_lines, sizeof(Line), line_comp);  //sort as usual
    MY_QSORT(rev_sorted_lines, (size_t) converted_lines, sizeof(Line), rev_line_comp);  //sort reversed
    print_lines(lines, (size_t) converted_lines); //print original lines
    print_lines(sorted_lines, (size_t) converted_lines); //print sorted lines
    print_lines(rev_sorted_lines, (size_t) converted_lines); //print reverse sorted lines
    free(text); //free memory
    free(lines);
    free(sorted_lines);
    free(rev_sorted_lines);
    return 0;
}

