#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <cstring>
#include "str_utils.h"
#define UTF8_FIRST(c) (((c) >> 6) != 2) //unsigned char only
/*!
 \file str_utils.c In this file functions from str_utils.h are implemented
 */

/*!
  Swaps two objects pointed by a and b, each size bytes
  \param[in] a pointer to the first object
  \param[in] b pointer to the second object
  \param[in] size size in bytes
*/
static void swp(void* a, void* b, size_t size) {
    void* tmp = calloc(1, size);
    memcpy(tmp, a, size);
    memcpy(a, b, size);
    memcpy(b, tmp, size);
    free(tmp);
}

/*!
 * Function, equivalent to qsort from stdlib.h
 \param[in] base pointer to the array
 \param[in] num the length of the array
 \param[in] size size of each element of the array in bytes
 \param[in] compar a comparator, which return -1 if <, 0 if ==, 1 if >
*/
void my_qsort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*)) {
    if (num < 2) return;
    char* arr = (char*) base;
    size_t split = 0;
    for (size_t i = 1; i < num; ++i) {
        if (compar(arr + split * size, arr + i * size) == 1) {
            swp(arr + split * size, arr + split * size + size, size);
            ++split;
            if (split != i)
                swp(arr + split * size - size, arr + i * size, size);
        }
    }
    qsort(arr, split, size, compar);
    qsort(arr + split * size + size, num - split - 1, size, compar);
}



/*!
  \brief This functions compares Lines lexicographically
  \param[in] a a pointer to the first Line
  \param[in] b a pointer to second Line
  \return -1 if a ->  str < b -> str , 1 if b -> str < a -> str and 0 if a -> str == b -> str
  This function works fine with Unicode as if first_str[i] != second_str[i] then we can say which
  string is less due to Unicode properties.
  */
int line_comp(const void* a, const void* b) {
    const Line* first = (const Line*) a;
    const Line* second = (const Line*) b;
    const unsigned char* first_str = (const unsigned char*) (first -> str);
    const unsigned char* second_str = (const unsigned char*) (second -> str);
    for (size_t i = 0; i < first -> len && i < second -> len; ++i) {
        if (first_str[i] < second_str[i]) return -1;
        if (first_str[i] > second_str[i]) return 1;
    }
    if ((first -> len ) < (second -> len)) return -1;
    if ((first -> len ) > (second -> len)) return 1;
    return 0;
}

/*!
  \brief gets next byte index as if we were reading l in reversed order
  \param[in] l a Line we are reading
  \param[in] i current position
  \return the next position or -1 if there is none
  */
static ssize_t get_rev_next(Line l, ssize_t i) {
    const unsigned char* str = (const unsigned char*) l.str;
    if (i + 1 == (ssize_t) l.len || UTF8_FIRST(str[i + 1])) {
        while (i > 0 && !UTF8_FIRST(str[i])) --i; //i to the start of current letter
        do {
            --i;                                //This construction behaves well if after first while
        } while (i > 0 && !UTF8_FIRST(str[i])); //i = 0. Then the second do-while sets i = -1 and then it is returned.
        return i;
    }
    return i + 1;
}

/*!
  \brief gets first byte in l as we were reading it reversed
  \param[in] l a Line
  \return position of the firsr byte in reversed order. If there is none, -1
  */
static ssize_t get_rev_start(Line l) {
    const unsigned char* str = (const unsigned char*) l.str;
    ssize_t i = (ssize_t) l.len - 1;
    for (;(!UTF8_FIRST(str[i])) && i > -1; --i);
    return i;
}

/*!
  This functions compares reversed Lines lexicographically
  \param[in] a a pointer to the first Line
  \param[in] b a pointer to second Line
  \return -1 if reversed a ->  str < reversed b -> str , 1 if reversed b -> str < reversed a -> str and 0 if a -> str == b -> str
  */
int rev_line_comp(const void* a, const void* b) {
    const Line* first = (const Line*) a;
    const Line* second = (const Line*) b;
    const unsigned char* first_str = (const unsigned char*) first -> str;
    const unsigned char* second_str = (const unsigned char*) second -> str;
    ssize_t i = get_rev_start(*first); //No error handling because we have to compare strings anyway.
    ssize_t j = get_rev_start(*second);
    while (true) {
        if (first_str[i] < second_str[j]) return -1;
        if (first_str[i] > second_str[j]) return 1;
        i = get_rev_next(*first, i);
        j = get_rev_next(*second, j);
        if (i == -1 || j == -1) break;
    }
    if ((first -> len ) < (second -> len)) return -1;
    if ((first -> len ) > (second -> len)) return 1;
    return 0;
}

/*!
  \brief reads the whole file from the stream, adjusting the target string if needed
  \param[in] n a pointer to the size of the string. 
  \param[in] stream input stream
  \return NULL pointer if an error has occurred or if we approached EOF before reading any character; pointer to the first symbol otherwise.

  Reads symbols from stream until approaches EOF and return them stored as str.
  Writes the number of read bytes to n. Does not add '\0' to the end, so str is not a valid C string.
  \warning str has to be freed.
  */
char* file_to_str(size_t *n, FILE *stream) {
    if (n == NULL) return NULL;
    struct stat file_stats;
    if (fstat(fileno(stream), &file_stats)) return NULL;
    long int sz = file_stats.st_size;
    if (sz < 0) return NULL;
    *n = (size_t) sz;
    char* str = (char*) calloc(*n, 1);
    fread(str, 1, *n, stream);
    if (ferror(stream)) {
        free(str);
        return NULL;
    }

    return str;
}

/*!
  This function takes C string and converts it to array of Lines.
  \param[in] str A C string
  \param[in] lines A pointer to an array of Lines of size *n which can be set free() or a pointer to NULL
  \param[in] n a pointer to the size of lines
  \param[in] len length of the string
  \return number of read lines or negative value in case of failure. Blank line is considered a line.
  \warning The data is not copied from the string! That means you cannot free() str until you are done working with lines.
  */
ssize_t str_to_lines(const char* str, size_t len, Line** lines, size_t* n) {
    if (n == NULL || lines == NULL || str == NULL) return -1;
    if (*n == 0) *n = 1;
    if (*lines == NULL ){
        *lines = (Line*) calloc(*n, sizeof(Line));
    }
    ssize_t current_line = 0;
    const char* left = str;
    for (const char* i = str; i < str + len ; ++i) {
        if ((size_t)(current_line) >= *n ) { //Reallocate enough memory to store lines 
            (*n) *= 2;
            Line* reallocated_ptr = (Line*) realloc(*lines, (*n)*sizeof(Line));
            if (reallocated_ptr == NULL) return -1;
            *lines = reallocated_ptr;
        }
        if (*i == '\n' || *i == '\0') {
            if (i == left) break;
            (*lines)[current_line].str = left;
            // not correct 
            (*lines)[current_line].len = (size_t)(i - left); //*i is not included
            ++current_line; //blank line is considered a line
            left = i + 1;
        }
    }
    return current_line;
}
